<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{asset('css/app.css')}}" rel="stylesheet" />
        <link href="{{asset('css/admin/fontawesome-free/css/all.min.css')}}" rel="stylesheet" />
        {{--admin style--}}
        <link href="{{asset('css/admin/sb-admin.css')}}" rel="stylesheet" />

        <title>Laravel Admin Panel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
        </style>
    </head>
    <body id="page-top">
    <div id="root"></div>

    <script src="{{asset('js/app.js')}}" ></script>
    <script src="{{asset('js/admin/sb-admin.js')}}" ></script>
    </body>
</html>
