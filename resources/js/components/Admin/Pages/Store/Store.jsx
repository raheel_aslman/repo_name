import React, {Component} from 'react';
import {HashRouter, Link, Route, Switch} from 'react-router-dom';
import swal from 'sweetalert';
import StoreListing from '../Store/sub-pages/Store/StoreListing';
import {AddStore} from "../Store/sub-pages/Store/AddStore";

class Store extends Component {
    state = {
      currentPage:'StoreListing',
      storeId: '',
      isEdit: false

    };
    changePage = (pageName) =>{
        this.setState({
           currentPage: pageName,
            isEdit:false
        });
    };
    editStore = (storeId) => {

        this.setState((prevState) => ({
            storeId: storeId,
            currentPage: 'AddStore',
            isEdit:true
        }));
    };
    removeStore = (storeId) => {
        let stores = JSON.parse(localStorage.getItem('storeData'));
        let storeObj = stores[storeId];
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    let storeData = stores.filter( (elem,i) => ( i != storeId ) );
                    localStorage.setItem('storeData', JSON.stringify(storeData));
                    this.setState({
                        currentPage:'StoreListing'
                    })
                    swal("Poof! Your store has been deleted!", {


                        icon: "success",
                    });
                } else {
                    swal("Your store is safe!");
                }
            });
    };
    loadComponent() {
        switch (this.state.currentPage) {
            case 'StoreListing' :
                return <StoreListing changePage={this.changePage}  editStore={this.editStore}  removeStore={this.removeStore}/>;
            case 'AddStore':
                return <AddStore changePage={this.changePage} storeId={this.state.storeId} isEdit={this.state.isEdit} />;
            default:
                return 'blank';
        }
    }
    render() {
        return(
         <React.Fragment>
             {this.loadComponent()}
         </React.Fragment>
        );
    }
}
export default Store;