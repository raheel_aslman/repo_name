import React, {Component} from 'react';

export default class StoreListing extends Component {

    storeData = JSON.parse(localStorage.getItem('storeData'));
    state = {
        keyword: '',
        currentPage: 1,
        perPage: 10,
        stores: [],
        allStore: [],
        perPageItems: []
    };
    componentDidMount = () => {
        this.storeRender();
        if (localStorage.getItem("storeData") == null)
            localStorage.setItem("storeData", JSON.stringify([]));

        this.setState(() => ({
            allStore: this.storeData
        }));
    };
    storeRender = () => {
        // filter store on base of name and email
        let filterStores = this.filterStore(this.storeData);
        // display pagination current store data
        let storeArray = this.paginationIndex(filterStores);
        // display page number in store listing table
        let perPageArray = this.pageNumber();
        this.setState((prevState) => ({
            stores: storeArray,
            perPageItems: perPageArray
        }));
    };
    searchStore = (e) => {
        this.setState((() => ({
            keyword: (e.target.value)
        }))(e), () => {
            this.storeRender();
        });

    };
    handlePaginaton = (event) => {
        this.setState((() => ({
            currentPage: Number(event.target.id)
        }))(event), () => {
            this.storeRender();
        });
    };
    perPageShow = (event) => {
        this.setState((() => ({
            perPage: Number(event.target.value)
        }))(event), () => {
            this.storeRender();
        });
    };

    filterStore = (stores) => {
        let filterStores = [];
        if(stores) {
             filterStores = stores.filter((store) => ((store.name.toLowerCase().search(this.state.keyword.toLowerCase()) !== -1) || (store.email.toLowerCase().search(this.state.keyword.toLowerCase()) !== -1)))
        }
        return filterStores;
    };
    paginationIndex = (filterStores) => {
        let lastStoreIndex = this.state.currentPage * this.state.perPage;
        let firstStoreIndex = lastStoreIndex - this.state.perPage;
        let storeArray = filterStores.slice(firstStoreIndex, lastStoreIndex);
        return storeArray;
    };
    pageNumber = () => {
        let perPageArray = [];
        let storeStateData = (this.storeData) ? this.storeData.length : 0;
        for (let i = 1; i <= Math.ceil(storeStateData / this.state.perPage); i++) {
            perPageArray.push(i);
        }
        return perPageArray;
    };

    render() {
        return (
            <div id="content-wrapper">
                <div className="container-fluid">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li className="breadcrumb-item active">Store list</li>
                    </ol>
                    <div className="row">
                        <div className="col-md-2">
                            <select className="custom-select mr-sm-2" onChange={this.perPageShow}>
                                <option defaultValue value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <input type="text" value={this.state.keyword} className="form-control"
                                       onChange={(e) => {
                                           this.searchStore(e)
                                       }} placeholder="search data"/>
                            </div>
                        </div>
                        <div className="col-md-6 col-md-offset-4">
                            <div className="col-md-12">
                                <a href="javascript:void(0)" onClick={() => {
                                    this.props.changePage('AddStore')
                                }} className="btn btn-primary float-right">Add New</a>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.state.stores != null && this.state.stores.map((store, key) => (
                                    <tr key={key}>
                                        <td>{store.name}</td>
                                        <td>{store.phone}</td>
                                        <td>{store.email}</td>
                                        <td>{store.contact}</td>
                                        <td>
                                            <a href="javascript:void(0)" onClick={() => {
                                                this.props.editStore(key)
                                            }}><i className="far fa-edit"></i></a> {'  '}
                                            <a href="javascript:void(0)" onClick={() => {
                                                this.props.removeStore(key)
                                            }} className=""><i className="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                ))
                                }
                                {this.state.stores == null && (<tr>
                                    <td colSpan={5}>No Record Found</td>
                                </tr>)}

                                </tbody>
                            </table>
                            <nav aria-label="Page navigation example">
                                <ul className="pagination justify-content-center">
                                    <li className="page-item disabled">
                                        <a id="1" onClick={this.handlePaginaton} className="page-link"
                                           href="javascript:void(0)" tabIndex="-1">Previous</a>
                                    </li>
                                    {
                                        this.state.perPageItems.length != 0 &&
                                        this.state.perPageItems.map((number, key) => (
                                            <li className="page-item" key={key}>
                                                <a id={number} onClick={this.handlePaginaton}
                                                   className="page-link" href="javascript:void(0)">{number}</a>
                                            </li>
                                        ))
                                    }
                                    <li className="page-item">
                                        <a className="page-link" id={this.state.perPage.length}
                                           onClick={this.handlePaginaton} href="javascript:void(0)">Last</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}