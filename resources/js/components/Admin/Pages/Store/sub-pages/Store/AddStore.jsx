import React, {Component} from 'react';

export class AddStore extends Component {

    state = {
      name:'',
      phone:'',
      email:'',
      contact:'',
    };
    changeStore = (e) => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]:value
        });
    };
    onSubmit = (event) => {
       event.preventDefault();
       event.target.className +=' was-validated';
       event.target.reset();
       this.saveStore();
    };
    componentDidMount = () => {
        if (this.props.isEdit) {
            let stores = JSON.parse(localStorage.getItem('storeData'));
            let storeObj = stores[this.props.storeId];
            this.setState({
                ...storeObj
            });

        } else {
            this.setState(
                {
                    name: '',
                    phone: '',
                    email: '',
                    contact: '',
                }
            );
        }
    };

    saveStore = () => {
        let storeArray = JSON.parse(localStorage.getItem("storeData"));
        if (this.props.isEdit) {
            let storeObject = storeArray[this.props.storeId];
            storeObject.name = this.state.name;
            storeObject.phone = this.state.phone;
            storeObject.email = this.state.email;
            storeObject.contact = this.state.contact;
            localStorage.setItem('storeData', JSON.stringify(storeArray));
        } else {
            storeArray.push({
                name: this.state.name,
                phone: this.state.phone,
                email: this.state.email,
                contact: this.state.contact
            });
            localStorage.setItem('storeData', JSON.stringify(storeArray));
        }
        this.props.changePage('StoreListing');
    };
    render() {
        return (
            <div id="content-wrapper">
                <div className="container-fluid">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li className="breadcrumb-item active">Create Store</li>
                    </ol>
                    <div className="card-body">
                        <form className="was-validated" onSubmit={this.onSubmit}   noValidate >
                            <div className="form-group row">
                                <label htmlFor="storeName" className="col-sm-2 col-form-label">Name</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="storeName"
                                           placeholder="Store Name" value={this.state.name}  name="name" required  onChange={(e)=> {this.changeStore(e,'name')}} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="phone" className="col-sm-2 col-form-label">Phone</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="phone"
                                           placeholder="Store Phone" value={this.state.phone} name="phone"  onChange={(e)=> {this.changeStore(e,'phone')}}required/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="email" className="col-sm-2 col-form-label">Email</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="email"
                                           placeholder="Store Email" name="email"  value={this.state.email} onChange={(e)=> {this.changeStore(e,'email')}} required/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor="contact" className="col-sm-2 col-form-label">Contact</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="contact"
                                           placeholder="Store Contact" name="contact"  value={this.state.contact} onChange={(e)=> {this.changeStore(e,'contact')}} required/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label"></label>
                                <div className="col-sm-10">
                                    <input type="submit" value="Save" className="btn btn-primary"/> { '  ' }
                                    <button type="button" onClick={() => {this.props.changePage('StoreListing')} } className="btn btn-danger">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        )
    }
}