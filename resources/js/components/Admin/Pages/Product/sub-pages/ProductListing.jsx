import React , { Component } from 'react';

class ProductListing extends Component {


    render() {
        return (
            <div id="content-wrapper">
                <div className="container-fluid">
                    <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li className="breadcrumb-item active">Product list</li>
                    </ol>
                    <div className="row">
                        <div className="col-md-2">
                            <select className="custom-select mr-sm-2" onChange={this.perPageShow}>
                                <option defaultValue value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                                <option value="20">20</option>
                            </select>
                        </div>
                        <div className="col-md-4">
                            <div className="form-group">
                                <input type="text" value="" className="form-control"
                                       onChange={(e) => {
                                           this.searchStore(e)
                                       }} placeholder="search data"/>
                            </div>
                        </div>
                        <div className="col-md-6 col-md-offset-4">
                            <div className="col-md-12">
                                <a href="javascript:void(0)" onClick={() => {
                                    this.props.changePage('AddStore')
                                }} className="btn btn-primary float-right">Add New</a>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default ProductListing;