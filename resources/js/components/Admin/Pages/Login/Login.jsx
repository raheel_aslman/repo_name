import React, { Component } from 'react';
import { NotificationContainer ,NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import { Link, withRouter } from "react-router-dom";



class Login extends Component{

    constructor (props) {
        super();

    }
    userEmail = "raheelaslam@gmail.com";
    userPassword = 123456;
    state = {
        email:'',
        password:'',
        isRemember:false,
        isEmailValid:false
    };
    componentDidMount() {
        this.isLoggedIn();
    }
    handleEmailChange = (event) => {
        this.setState({
            email:event.target.value,
            isEmailValid:true
        })
    }; //..... end of handleEmailChange ..... //
    handlePasswordChange = (event) => {
        this.setState({
           password:event.target.value
        });
    }; //.....end of handlePasswordChange .....//
    handleCheckbox = () => {
        this.setState({
            isRemember:event.target.checked
        });
    }; //.... end of handleCheckbox ..... //
    isLoggedIn = () => {
        let authenticated = localStorage.getItem('isAuthenticated');
        if(authenticated && authenticated === 'true' ) {
            let from = '/';
             this.props.history.push(from);
        }

    }; // ..... end of isLoggedIn .....//
    handleSignClick = () => {
        if(!this.state.isEmailValid) { console.log('Please provide the valid email address');
            NotificationManager.error('Please provide the valid email address', 'Error',3000);
            return false;
        } // ..... end if
        if(this.state.password.length == 0) { console.log('Please provide the password');
            NotificationManager.error('Please provide the password','Error');
            return false;
        } // ..... if
        if(this.userEmail == this.state.email || this.userPassword == this.state.password) {
            let userData = {email:this.state.email,password:this.state.password};
            this.persistUserDetails(userData);
        }else{
            NotificationManager.error('Please provide the correct detail','Error');
            return false;
        }


    }; // ..... end of handlePasswordChange ..... //
    persistUserDetails = (data) => {
        localStorage.setItem('isAuthenticated','true');
        localStorage.setItem('userData',JSON.stringify(data));
        localStorage.setItem('isRemember',this.state.isRemember ? 'true': 'false');
        localStorage.setItem('loggedInTime',(new Date()).getTime());
        this.isLoggedIn();
    };

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <div className="card card-signin my-5">
                            <div className="card-body">
                                <h5 className="card-title text-center">Admin Panel | Sign In</h5>
                                <hr className="my-4" />
                                <form className="form-signin">
                                    <div className="form-group">
                                        <input type="email" id="inputEmail" onChange={this.handleEmailChange} className="form-control"
                                               placeholder="Email address"  autoFocus />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" id="inputPassword" onChange={this.handlePasswordChange} className="form-control"
                                               placeholder="Password"  />
                                    </div>
                                    <div className="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" className="custom-control-input" id="customCheck1" onClick={this.handleCheckbox} />
                                            <label className="custom-control-label" htmlFor="customCheck1">Remember
                                                password</label>
                                    </div>
                                    <button className="btn btn-lg btn-primary btn-block text-uppercase"
                                            type="button" onClick={this.handleSignClick} >Sign in
                                    </button>
                                    <hr className="my-4" />

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(Login);