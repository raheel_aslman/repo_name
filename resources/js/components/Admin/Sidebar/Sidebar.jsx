import React, {Component} from 'react';
import {Link,  NavLink, withRouter } from 'react-router-dom';
import Store from '../Pages/Store/Store';
import Header from "../Header/Header";
import Dashboard from "../Dashboard/Dashboard";

class Sidebar extends Component {
    render() {
        return (
            <div id="wrapper">

                    <div>
                        <ul className="sidebar navbar-nav">
                            <li className="nav-item active">
                                <Link to="/"  className="nav-link"  replace >
                                    <i className="fas fa-fw fa-tachometer-alt"></i>
                                    <span> Dashboard</span>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/stores"  onClick={()=> {window.location = '#/stores'} } className="nav-link"  replace >
                                    <i className="fas fa-fw fa-folder"></i>
                                    <span> Stores</span>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/categories"  className="nav-link" replace >
                                    <i className="fas fa-fw fa-chart-area"></i>
                                    <span> Categories</span>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/products"  className="nav-link" replace >
                                    <i className="fas fa-fw fa-table"></i>
                                    <span> Products</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
            </div>
        )
    }
}
export default withRouter(Sidebar);