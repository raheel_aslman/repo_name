import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Switch, BrowserRouter, Link, NavLink} from 'react-router-dom';

import {NotificationContainer} from 'react-notifications';
import Header from '../components/Admin/Header/Header';
import Sidebar from '../components/Admin/Sidebar/Sidebar';
import Dashboard from '../components/Admin/Dashboard/Dashboard';
import Store from "./Admin/Pages/Store/Store";
import Product from './Admin/Pages/Product/Product';
import Login from '../components/Admin/Pages/Login/Login';


export default class Master extends Component {
    constructor(props) {
        super(props);
    }
    isAuthenticated = localStorage.getItem('isAuthenticated');
    state = {
        isLoggedIn: false
    };
    componentDidUpdate() {
        this.setState({
            isLoggedIn: (this.isAuthenticated == 'true' ? true : false)
        })
    }
    render() { console.log("working component did update");  console.log(this.state.isLoggedIn);
        return (
            <React.Fragment>
                <HashRouter>
                    <div>
                        <Header/>
                        <div id="wrapper">
                            { this.state.isLoggedIn == false &&
                                        <Sidebar/>
                            }
                            <div id="content-wrapper">
                                <Switch>
                                    <Route path="/" component={Dashboard} exact/>
                                    <Route path="/stores" component={Store}/>
                                    <Route path="/products" component={Product}/>
                                    {this.state.isLoggedIn === false &&
                                    <Route exact path={'/login'} component={Login}/>
                                    }
                                </Switch>
                            </div>
                        </div>
                    </div>
                </HashRouter>
                <NotificationContainer/>
            </React.Fragment>
        );
    }
}

if (document.getElementById('root')) {
    ReactDOM.render(<Master/>, document.getElementById('root'));
}
